(function (window, document, JSON){
    

    const context = "/webSocket";
    const endPoint = "/chat";

    const url = "ws://" + window.location.host + context + endPoint;
    console.log(url);
    let ws = new WebSocket(url);
    let mensajes = document.getElementById("conversacion");
    let boton = document.getElementById("btnEnviar");
    let nombre = document.getElementById("txtUsuario");
    let mensaje = document.getElementById("txtMensaje");
    

    // ASIGNAR LAS FUNCIONES COMO EN LA CLASE JAVA "CHAT"
    ws.onopen = onOpen;
    ws.onclose = onClose;
    ws.onmessage = onMessage;

    boton.addEventListener('click', enviar);
   

    function onOpen() {
        console.log("Conectado");
    }
    function onClose() {
        console.log("Desconectado");
    }
    function onMessage(evt) {
        let obj = JSON.parse(evt.data);
        let msg = "Nombre: " + obj.nombre + " dice: " + obj.mensaje;
        mensajes.innerHTML += "<br/>" + msg;
    }
    function enviar(){
        var json = {
            nombre: nombre.value,
            mensaje: mensaje.value
        };
        ws.send(JSON.stringify(json));
        mensaje.value = "";
        mensaje.focus();
    }


//    let enviar = () => {
//        var json = {
//            nombre: nombre.value,
//            mensaje: mensaje.value
//        };
//
//        ws.send(JSON.stringify(json));
//    };
//
//    let onOpen = () => console.log("Conectado");
//
//    let onClose = () => console.log("Desconectado");
//
//
//
//    let onMessage = (evt) => {
//        // Obtener la informacion del evento
//        let obj = JSON.parse(evt.data);
//        let msg = "Nombre: " + obj.nombre + " dice: " + obj.mensaje;
//        mensajes.innerHTML += "<br/>" + msg;
//    };


})(window, document, JSON);
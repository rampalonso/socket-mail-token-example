<%-- 
    Document   : index
    Created on : 17/12/2017, 11:48:08 AM
    Author     : Alonso
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Nuestro chat!</h1>
        <div>
            <div>
                <label>Nombre: </label>
                <input type="text" id="txtUsuario" placeholder="usuario"/>
            </div>
            <div>
                <label>Mensaje: </label>
                <textarea id="txtMensaje"></textarea>
            </div>
            <div>
                <button id="btnEnviar">Enviar</button>
            </div>
            <div id="conversacion"></div>
        </div>
        <script src="../js/socket.js" type="text/javascript"></script>
    </body>
</html>

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package websocket.mail;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;

/**
 *
 * @author Alonso
 */
public class SendMail {

    public void EnviarCorreo() {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
            new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("username", "password");
            }
        });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("username"));
            message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse("to"));
            message.setSubject("PRUEBA02");
            message.setText("PRUEBA DE ENVIO DE CORREOS ELECTRONICOS");

            Transport.send(message);
            System.out.println("Mensaje enviado");

        } catch (MessagingException e) {

            System.out.println("No se envio prro");
        }
    }

    public static void main(String[] args) {
        SendMail s = new SendMail();
        s.EnviarCorreo();
    }
}

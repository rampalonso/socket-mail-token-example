/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package websocket.chat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import webscoket.objeto.DecoderMensaje;
import webscoket.objeto.EncoderMensaje;
import webscoket.objeto.Mensaje;

/**
 *
 * @author Alonso
 */

// @ServerEndPoint => SIRVE PARA QUE EL SERVIDOR ESTE PENDIENTE DE LO QUE OCURRA EN ESTA CLASE
// value => URI , 
// encoders = Clase(s) encargadas del encoding. (Se puede pasar un array de clases)
// decoders = Clase(s) encargadas del decoding. (Se puede pasar un array de clases)
@ServerEndpoint(value="/chat", encoders = {EncoderMensaje.class}, decoders = {DecoderMensaje.class})
public class Chat {
    
    // Lista de sesiones en el endPoint
    private static final List<Session> conectados = new ArrayList<>();
    
    // SE ENCARGA DE REALIZAR UNA TAREA CUANDO ALGUIEN SE CONECTE
    @OnOpen
    public void onOpen(Session session){
        conectados.add(session);
        System.out.println("Sesion conectada: " +session);
    }
    
    // SE ENCARGA DE REALIZAR UNA TAREA CUANDO ALGUIEN SE DESCONECTE
    @OnClose
    public void onClose(Session session){
        conectados.remove(session);
    }
    
    // SE ENCARGA DE ENVIAR LA INFORMACION DE UN CLIENTE A OTRO CLIENTE
    @OnMessage
    public void onMessage(Mensaje mensaje) throws IOException, EncodeException{
        
        // FOR EACH => PARA RECORRER LA LISTA DE LOS CLIENTES CONECTADOS, Y ENVIAR LOS MENSAJES RECIBIDOS
        for (Session conectado : conectados) {
            // Enviar el objeto
            conectado.getBasicRemote().sendObject(mensaje);
            System.out.println("mensaje: "+mensaje.getMensaje());
        }
        
    }
    
}

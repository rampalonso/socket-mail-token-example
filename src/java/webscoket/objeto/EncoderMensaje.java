/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webscoket.objeto;

import java.io.IOException;
import java.io.Writer;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

/**
 *
 * @author Alonso
 */
// CONVIERTE LA CLASE (MENSAJE) EN UN OBJETO JSON
public class EncoderMensaje implements Encoder.TextStream<Mensaje> {

    // SE ENCARGA DE CONVERTIR LA CLASE EN UN OBJETO JSON
    @Override
    public void encode(Mensaje object, Writer writer) throws EncodeException, IOException {
        // https://docs.oracle.com/javaee/7/api/javax/json/JsonObjectBuilder.html
        JsonObject json
            = Json.createObjectBuilder()
                .add("nombre", object.getNombre())
                .add("mensaje", object.getMensaje())
                .build();
        
        // https://docs.oracle.com/javaee/7/api/javax/json/JsonWriter.html
        // Writer: Se encargará de enviar la inforamción
        // try("Auto-closeable") => Se realiza la instancia el el try,
        // y ya no se necesita definir un cierre del objeto
        try(JsonWriter jsonWriter = Json.createWriter(writer)) {
            jsonWriter.writeObject(json);
        } catch (Exception e) {
        }
    }

    @Override
    public void init(EndpointConfig config) {
    }

    @Override
    public void destroy() {
    }

}

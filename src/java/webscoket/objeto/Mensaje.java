/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webscoket.objeto;

import java.io.Serializable;

/**
 *
 * @author Alonso
 */

// ESTE OBJETO SE TIENE QUE CONVERTIR EN UN OBJETO JSON
// Y EL CLIENTE ENVIA UN OBJETO JSON Y SE TIENE QUE CONVERTIR EN  CLASE
public class Mensaje implements Serializable{
    
    private String nombre;
    private String mensaje;

    public Mensaje() {
    }

    
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webscoket.objeto;

import java.io.IOException;
import java.io.Reader;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

/**
 *
 * @author Alonso
 */
public class DecoderMensaje implements Decoder.TextStream<Mensaje>{

    
    // SE ENCARGA DE LEER EL OBJETO JSON Y LO CONVIERTE EN UNA CLASE
    @Override
    public Mensaje decode(Reader reader) throws DecodeException, IOException {
        Mensaje mensaje = new Mensaje();
        
        try (JsonReader jsonReader = Json.createReader(reader)) {
            JsonObject json = jsonReader.readObject();
            mensaje.setNombre(json.getString("nombre"));
            mensaje.setMensaje(json.getString("mensaje"));
        } catch (Exception e) {
        }
        
        return mensaje;
    }

    @Override
    public void init(EndpointConfig config) {
       
    }

    @Override
    public void destroy() {
        
    }
    
    
    
}
